SHELL=/bin/bash

all: basics clean

basics:
	pandoc basics.md -o basics.tex --number-sections --template template.latex
	sed -i 's/begin{figure}/begin{figure}\[H\]/g' basics.tex
	lualatex basics.tex
	lualatex basics.tex
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 \
		 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH \
		 -sOutputFile=small_basics.pdf basics.pdf
	mv small_basics.pdf basics.pdf

clean:
	rm -f basics.{aux,glg,glo,gls,ist,lof}
	rm -f basics.{log,out,toc,glsdefs,idx,ilg,ind}

cclean:
	rm -f basics.{pdf,tex}
