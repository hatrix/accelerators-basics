---
author: Maël Le Garrec
title: What a CS Engineer thinks he understands about accelerator physics
subtitle: Basics
location: CERN
team: BE-ABP-HSS / OMC
...


[^1]: https://lhc-machine-outreach.web.cern.ch/lhc-machine-outreach/components/magnets/types_of_magnets.htm
[^2]: https://indico.cern.ch/event/509762/contributions/2711716/attachments/1520600/2880647/nlb.pdf


# Generalities

The LHC collides particles, either protons or lead.
Those particles are part of a _beam_ and split in several _bunches_.
Those bunches describe several (>10k) turns before colliding.

* RF Cavities: cavities used to confine electromagnetic fields in order to
accelerate the particles.
* IR: Interraction Region, they are 8 in the LHC
* IP: Interraction Point, 8 also, but only 4 where collisions happen
* LSA: Database where settings are stored
* BBQ: Base Band $Q$ (tune), passive real-time tune monitoring

# LHC Nomenclature

Several components are located in the LHC. To keep track of the position,
there's a special nomenclature.
For a BPM located on the 9th position on the left of the IP1 tracking the beam
1, you'd write:

$$ BPM.9R1.B1$$

\begin{center}
\begin{tabular}{ll}
 BPM & Hardware type (Magnet, BPM, …) \\
 9 & Position, 9th element \\
 R & Right or Left of …  \\
 1 & IP \#1 \\
 B1 & Beam (1 or 2)\\
\end{tabular}

$$ MCTX $$

\begin{tabular}{ll}
 M & Magnet \\
 C & Corrector (C or Q for regular magnet) \\
 T & Dodecapole \\
 X & located at IPs \\
\end{tabular}

\begin{tabular}{lllllll}
Magnet Name & B & Q & S & O & D & T \\
Number of Poles & 2 & 4 & 6 & 8 & 10 & 12 \\
b designation & 0 & 1 & 2 & 3 & 4 & 5 \\
\end{tabular}
\end{center}



![Left and Right are seen from the center of the ring](./images/irs.png)

# Magnets

The couple of magnets and their strength are refered to as Optics as they are
similar to lenses and light beams.  
If the powering of the magnets and thus strength changes, we have new optics.
There are a lot[^1] of different ones in the LHC and other accelerators.

* Dipoles: as the LHC is a circular collider, the particles need to be bent.
The magnets responsible for this bending are the dipoles
* Quadrupoles: they are used to keep the beam within the machine's aperture.
Quadrupoles focus in one plane but defocus in the other, that's why they are 
not used alone, but rather part of a cell
* FoDo cells: cells made of a quadrupole, void, quadrupole and void again.
Those are used to focus the beam and keep it within the accelerator's aperture.
* Skew quadrupoles: they correct coupling effects induced by other magnets
* Triplets: triplets are a kind of FODO cells used at interaction points. They
differ from them in that they focus more. They're used to lower the 
$\beta$-function to a maximum at the IPs
* Arcs: bent parts of the LHC. Composed of magnets, mainly dipoles and quads
* ATS: Achromatic Telescopic Squeezing. New configuration for magnets for 
higher luminosity at IPs. "Super" squeezing optics
* Ballistic Optics: term used when the triplets are not powered on
* Round Optics: name given to optics where $β_x = $β_y$

# Beam

* Dispersion: change of orbit with momentum, in the tranversal planes
* Coupling: energy transfer between the one transversal planes
* Coupling amplitude: strength of the coupling
* Emittance: area where the most part of the beam is in the phase space
* $\beta$-function: envelope of the beam (or single particule). $β^{ph}$:
β from phase, $β^{mdl}$: β from model.
* Beam size: depends on the emittance and the $\beta$-function
* Tune: number of oscillations of the $\beta$-function per turn in the
accelerator. This usually defined by $Q_{x,y}$ for both planes. Visible as
the main frequency in the spectrum. The tune's unit is 2π and only its
fractional part is useful
* Chromaticity: tune change with energy/momentum
* Aperture: physical, cross-section surface of the beam pipe
* Dynamic Aperture: particles outside the dynamic aperture become lost as time
increases.
* Betatron oscillation: oscillation in the transverse plane -> x and y
* Design orbit: orbit where physicists would like the particle to be. But 
accelerators are never perfect and that's never the case
* Reference orbit: orbit where the particle actually is. When using
coordinates, they usually use this space
* Waist: difference in meters of the location of $β^*$ and the IP. Waist shift:
shift induced to get the $β^*$ at the correct position
* RDT, Resonance Driving Term: something that has to do with Hamiltonian. 
\hl{Investigate}
* LBDS: LHC Beam Dump System
* Feedback loop: Stabilization method for RF. If the bunch isn't perfectly 
synchronized with the RF, the frequency is then changed a bit to match

\newpage 

## Spaces

* Phase space: two axis space, usually position and momentum. Sometimes
position and velocity. It describes all the possible states of a system.  
The labels of the axis are ($x, p_x$) or ($x, \dot{x}$)

![Phase space of a pendulum](./images/phase_space.png)

* Trace space: two axis also but with position and angles. Mainly used for
linacs and beam lines where the particles go through only once[^2]. The labels
are ($x, x'$) with $x' = \frac{dx}{ds}$.

## Measurement

* Beam Position Monitor (BPM): gets the position of the center of the bunch
* Kick: doing a small kick with a dipole allows to oscillate slightly the bunch.
Done with AC dipoles
* Peak 2 Peak: 2x amplitude of the $\beta$-function. We are particularly
interested on those values in the ATS arcs.
* BSRT: Beam Synchrotron Radiation Telescope. Gives an image of the beam via
the emitted photons. Size and intensity can be known
* n-BPM: phase advance calculation. \hl{Investigate}
* Monte Carlo: phase advance calculation. \hl{Investigate}

Depending on the resulting oscillations after a kick, the beam size can be
determined.

## Corrections

* Knob: group of settings to be applied on hardware

Resonance is an effect created by the machine at certain tunes. This effect may
damage the machine or lose a whole bunch as is creates strong oscillations of
the beam.
A resonance diagram can be used to find safe fractional tunes.

![Safe spots for a tune in both planes are indicated in white](./images/resonance.png)

\newpage 

# Some equations

* _Particule deviation in a dipole_ $$ sin\frac{Θ}{2} = \frac{L}{2ρ} = \frac{1}{2} · \frac{LB}{(Bρ)}$$

  * $L$: length of the dipole (m)
  * $B$: field of the dipole (T)
  * $Θ$: angle the particle is deviated (rad)
  * $Bρ$: magnetic rigidity, constant: 3.3356p (T·m) where p is the momentum (GeV/c)
  \bigbreak
  * If $θ$ is small, then $sin\frac{Θ}{2} ≃ Θ$ and $Θ = \frac{LB}{(Bρ)}$

* _Gradient of a quadrupole, calculated for each plane_: $$K = \frac{dB_y}{dx}$$
  * $K$: gradient ($\mathrm{T·m^{-1}}$)
  * $B_y$: field of the quadrupole in the y plane (T)
  * $x$: position of the particule in the x plane (m)
  * normalised gradient: $k = \frac{K}{(Bρ)}$ ($\mathrm{m^{-2}}$)

* _Focal length of a quadrupole_: $$f = \frac{(Bρ)}{KL}$$
  * $f$: focal length (m)
  * $Bρ$: magnetic rigidity (T·m)
  * $K$: gradient of the quadrupole ($\mathrm{T·m^{-1}}$)
  * $L$: Length of the quadrupole (m)

* _Phase advance_: $$μ = \int_A^B \frac{1}{β(s)}\, ds$$
  * $μ$: phase advance from point A to point B
  * $β(s)$: $β$-function in function of _s_

\newpage

* _Tune_: $$Q = \frac{μ}{2π}$$ or $$Q = \oint\frac{1}{2πβ(s)}\, ds$$
  * $Q$: tune for one element of the accelerator
  * $μ$: phase advance of the betatron oscillation (rad)


\newpage

* _Tune change due to quadrupole errors in a FODO cell_: \begin{equation*}
\begin{pmatrix}
ΔQ_v\\
ΔQ_h 
\end{pmatrix}
=
\frac{1}{4π}
\begin{pmatrix}
β_{VD} && -β_{VF}\\
-β_{HD} && β_{HF}
\end{pmatrix}
\begin{pmatrix}
Δk_D·l_D\\
Δk_F·l_F
\end{pmatrix}
\end{equation*}

  * $ΔQ_V$: tune change due to the quadrupole error in the vertical axis
  * $β_{VD}$: β-function in the vertical plane at the center of the defocusing
    quadrupole (m)
  * $Δk_D$: variation of the normalised gradient introduced by the defocusing magnet ($\mathrm{m^{-2}}$)
  * $l_D$: length of the defocusing quadrupole (m)

# IT Stuff

* RBAC: Access control for services used by/for the CCC. Used on applications
such as BetaBeat GUI to control knobs edition.
* FESA: C++ frontend to control hardware

## Beta-Beat

The Beta-Beat python code allows to do analysis and perform corrections on the
optics. The Java GUI is used to control inputs, outputs and display the
results.

### BPM

* BPM data is given as raw in .sdds files
* Some BPM can yield erroneous datas and need to be cleaned with thresholds or
later on with machine learning
* The BPM data represents the position of the beam in one transversal plane.
Given the BPM name, the longitudinal position is known.
* TBT data refers to Turn-By-Turn data

### Frequencies

The frequency is the tune.

* With the BPM data for each turn, frequencies of the beam can be calculated
via a FFT
* The main tune is the one with the maximum amplitude
* The second frequency is usually the main tune of the other plane due to
coupling. 

### \hlcyan{Optics ??}

What's this pannel used for?

### \hlcyan{Corrections ??}

What's this pannel used for?

## Multiturn

The Multiturn GUI is used to control the kicks and give some basic infos about
them.
Data is analysed as soon as it's available.


\label{End}
